-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 19 mai 2019 à 03:22
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `estival`
--

-- --------------------------------------------------------

--
-- Structure de la table `appartement`
--

DROP TABLE IF EXISTS `appartement`;
CREATE TABLE IF NOT EXISTS `appartement` (
  `n_appartement` int(11) NOT NULL AUTO_INCREMENT,
  `capacite` int(11) NOT NULL,
  `localisation` varchar(30) NOT NULL,
  `disponibilite` varchar(10) NOT NULL,
  `prix` float NOT NULL,
  PRIMARY KEY (`n_appartement`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `appartement`
--

INSERT INTO `appartement` (`n_appartement`, `capacite`, `localisation`, `disponibilite`, `prix`) VALUES
(1, 2, 'BLOC C ETAGE 2', 'oui', 300),
(2, 4, 'BLOC A RDC', 'oui', 500),
(3, 6, 'BLOC D ETAGE 2', 'oui', 600),
(4, 2, 'BLOC E ETAGE 3', 'oui', 280);

-- --------------------------------------------------------

--
-- Structure de la table `bungalow`
--

DROP TABLE IF EXISTS `bungalow`;
CREATE TABLE IF NOT EXISTS `bungalow` (
  `n_bungalow` int(11) NOT NULL AUTO_INCREMENT,
  `capacite` int(11) NOT NULL,
  `localisation` varchar(100) NOT NULL,
  `disponibilite` varchar(10) NOT NULL,
  `prix` float NOT NULL,
  PRIMARY KEY (`n_bungalow`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `bungalow`
--

INSERT INTO `bungalow` (`n_bungalow`, `capacite`, `localisation`, `disponibilite`, `prix`) VALUES
(1, 4, 'Agadir plage bloc C', 'oui', 400),
(2, 6, 'Agadir plage bloc D', 'oui', 650),
(3, 4, 'Agadir plage bloc A', 'oui', 500),
(4, 6, 'Agadir plage bloc A', 'oui', 600);

-- --------------------------------------------------------

--
-- Structure de la table `caravane`
--

DROP TABLE IF EXISTS `caravane`;
CREATE TABLE IF NOT EXISTS `caravane` (
  `n_caravane` int(11) NOT NULL AUTO_INCREMENT,
  `capacite` int(11) NOT NULL,
  `localisation` varchar(100) NOT NULL,
  `disponibilite` varchar(10) NOT NULL,
  `prix` float NOT NULL,
  PRIMARY KEY (`n_caravane`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `caravane`
--

INSERT INTO `caravane` (`n_caravane`, `capacite`, `localisation`, `disponibilite`, `prix`) VALUES
(1, 4, 'Marrakech site1', 'oui', 470),
(2, 6, 'Marrakech site1', 'oui', 700),
(3, 2, 'Marrakech site1', 'oui', 350),
(4, 6, 'Marrakech site1', 'non', 720);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`id_client`, `nom`, `username`, `password`) VALUES
(1, 'barry', 'sadio', 'barry'),
(2, 'gaouzi', 'zeynabe', 'gaouzi');

-- --------------------------------------------------------

--
-- Structure de la table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id_client` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(100) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `tel` int(14) NOT NULL,
  `pays` varchar(50) NOT NULL,
  `ville` varchar(50) NOT NULL,
  `adresse` varchar(150) NOT NULL,
  `dateArrivee` varchar(30) NOT NULL,
  `dateSortie` varchar(30) NOT NULL,
  `nbJour` int(11) NOT NULL,
  `etatFami` varchar(10) NOT NULL,
  `typeResto` varchar(50) NOT NULL,
  `numHebergement` int(11) NOT NULL,
  `typeHebergement` varchar(50) NOT NULL,
  PRIMARY KEY (`id_client`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `clients`
--

INSERT INTO `clients` (`id_client`, `nom`, `prenom`, `mail`, `tel`, `pays`, `ville`, `adresse`, `dateArrivee`, `dateSortie`, `nbJour`, `etatFami`, `typeResto`, `numHebergement`, `typeHebergement`) VALUES
(48, 'Barry', 'Abdoulaye Sadio', 'barry@barry.com', 212000000, 'Guinee', 'Dalaba', 'conakry guinee 224', '20-05-19', '22-05-19', 5, 'Monsieur', 'demie pension', 4, 'Caravane');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
