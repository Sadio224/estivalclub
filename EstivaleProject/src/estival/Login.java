package estival;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;
import java.sql.*;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout.Alignment;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Color;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.border.LineBorder;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;

public class Login extends JFrame {

	private JPanel contentPane;
	private JTextField textUsername;
	private JPasswordField passwordField;

	public static String email = null;
	public static String login = null;
	

	Connection conn = null;
	
	/**
	 * Create the frame.
	 */
	public Login() {
		
		Login log = this;
		
		conn = SqliteConnexion.dbConnector();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Image img = new ImageIcon(this.getClass().getResource("../back1.png")).getImage();
		setBounds(100, 100, 1024, 675);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(173, 216, 230));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Estival reservation");
		lblNewLabel.setFont(new Font("Andora", Font.BOLD | Font.ITALIC, 60));
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setBounds(511, 0, 464, 142);
		contentPane.add(lblNewLabel);
		
		JLabel lblLogin = new JLabel("Connexion");
		lblLogin.setFont(new Font("Century Gothic", Font.BOLD | Font.ITALIC, 50));
		lblLogin.setForeground(new Color(250, 128, 114));
		lblLogin.setBounds(623, 108, 286, 73);
		contentPane.add(lblLogin);
		
		JLabel lblUser = new JLabel("Login :");
		lblUser.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblUser.setForeground(new Color(0, 0, 0));
		lblUser.setBounds(573, 243, 129, 35);
		contentPane.add(lblUser);
		
		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setForeground(new Color(0, 0, 0));
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblPassword.setBounds(573, 320, 140, 31);
		contentPane.add(lblPassword);
		
		textUsername = new JTextField();
		textUsername.setBackground(new Color(255, 239, 213));
		textUsername.setFont(new Font("Tahoma", Font.PLAIN, 20));
		textUsername.setBounds(573, 272, 356, 45);
		contentPane.add(textUsername);
		textUsername.setColumns(10);
		
		passwordField = new JPasswordField();
		passwordField.setFont(new Font("Tahoma", Font.BOLD, 20));
		passwordField.setBackground(new Color(255, 239, 213));
		passwordField.setBounds(573, 348, 247, 45);
		contentPane.add(passwordField);
		
		////////////////////////
		JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Reservation", "Suivie"}));
		comboBox.setBackground(new Color(255, 239, 213));
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 20));
		comboBox.setBounds(754, 184, 175, 35);
		//comboBox.addItem("Réserver");
		//comboBox.addItem("Suivre réservation");
		//comboBox.addItem("Admin");
		contentPane.add(comboBox);
		//////////////////////
		
		JButton btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				//JOptionPane.showMessageDialog(null, "Connection Successful");
				//Estival.getFrame().setVisible(true);
				ResultSet rst = null;
				PreparedStatement pst = null;
				String query = null;
				switch(comboBox.getSelectedItem().toString()) {
					case "Reservation" : {
						try {
							login = textUsername.getText();
							query = "select * from client where username=? and password=?";
							pst = conn.prepareStatement(query);
							//Statement st = conn.createStatement();
							pst.setString(1, textUsername.getText());
							pst.setString(2, passwordField.getText());
							rst = pst.executeQuery();
							if(rst.next()) {
								Menu menu = new Menu();
								menu.setVisible(true);
								log.setVisible(false);
							}
							else {
								JOptionPane.showMessageDialog(null, "Veuillez vous enregistrer ");
								Estival.getFrame().setVisible(true);
							}
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null, e);
						}
						finally {
							try {
								rst.close();
								pst.close();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
							}
						}
					}break;
					case "Suivie" : {
						try {
							email = textUsername.getText();
							query = "select * from clients where mail=?";
							pst = conn.prepareStatement(query);
							//Statement st = conn.createStatement();
							pst.setString(1, textUsername.getText());
							rst = pst.executeQuery();
							if(rst.next()) {
								EspaceUtilisateur espu = new EspaceUtilisateur();
								espu.setVisible(true);
								log.setVisible(false);
							}
							else {
								JOptionPane.showMessageDialog(null, "Veuillez vous enregistrer ");
								Estival.getFrame().setVisible(true);
							}
						} catch (Exception e) {
							JOptionPane.showMessageDialog(null, e);
						}
						finally {
							try {
								rst.close();
								pst.close();
							} catch (SQLException e) {
								// TODO Auto-generated catch block
							}
						}
					}break;
					case "Admin" : {
						JOptionPane.showMessageDialog(null, "Service non defini");
					}break;
				}
			}
		});
		btnLogin.setForeground(new Color(0, 0, 0));
		btnLogin.setFont(new Font("Century Gothic", Font.BOLD, 25));
		btnLogin.setBackground(new Color(255, 239, 213));
		btnLogin.setBounds(658, 406, 195, 59);
		contentPane.add(btnLogin);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(img));
		label.setBounds(0, 0, 478, 633);
		contentPane.add(label);
		
		JLabel lblQueVoulezvous = new JLabel("Que d\u00E9sirez-vous ?");
		lblQueVoulezvous.setForeground(Color.BLACK);
		lblQueVoulezvous.setFont(new Font("Quentin", Font.PLAIN, 20));
		lblQueVoulezvous.setBounds(573, 187, 169, 35);
		contentPane.add(lblQueVoulezvous);
		
		JLabel lblcopyright = new JLabel("\u00A9Copyright 2019 - Estival r\u00E9servation.");
		lblcopyright.setForeground(Color.BLACK);
		lblcopyright.setFont(new Font("Verdana Pro Cond Light", Font.BOLD | Font.ITALIC, 15));
		lblcopyright.setBounds(623, 593, 296, 35);
		contentPane.add(lblcopyright);
		
		JCheckBox chckbxShow = new JCheckBox("Show");
		chckbxShow.setFont(new Font("Tahoma", Font.BOLD, 17));
		chckbxShow.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (chckbxShow.isSelected()) {
					passwordField.setEchoChar((char)0);
				}
				else {
					passwordField.setEchoChar('*');
				}
			}
		});
		chckbxShow.setBounds(831, 356, 98, 29);
		contentPane.add(chckbxShow);
		
		JButton button = new JButton("");
		button.setForeground(Color.WHITE);
		button.setFont(new Font("Times New Roman", Font.BOLD, 20));
		button.setContentAreaFilled(false);
		button.setBorder(new LineBorder(Color.WHITE, 2, true));
		button.setBackground(Color.LIGHT_GRAY);
		button.setBounds(530, 167, 457, 330);
		contentPane.add(button);
		                                    
	}
}
