package estival;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Color;
import java.awt.SystemColor;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Detail extends JFrame {

	private JPanel contentPane;
	private JTextField txtAppartement;
	private JTextField textFieldCapacite;
	private JTextField textFieldTarif;
	private JTextField textFieldDispo;
	private JTextField textFieldLocali;


	/**
	 * Create the frame.
	 */
	public Detail() {
		
		Detail detailFram = this;
		
		Image img = new ImageIcon(this.getClass().getResource("../background.png")).getImage();
		Image img4 = new ImageIcon(this.getClass().getResource("../back.png")).getImage();
		Image img1 = null;
		Image img2 = null;
		Image img3 = null;
		switch(Menu.type) {
			case "Appartement" : {
				
				img1 = new ImageIcon(this.getClass().getResource("../bed1.jpg")).getImage();
				img2 = new ImageIcon(this.getClass().getResource("../bed2.jpg")).getImage();
				img3 = new ImageIcon(this.getClass().getResource("../bed3.jpg")).getImage();
				
			}break;
			case "Bungalow" : {
				img1 = new ImageIcon(this.getClass().getResource("../bungalowChamb3.jpg")).getImage();
				img2 = new ImageIcon(this.getClass().getResource("../bungalowChamb2.jpg")).getImage();
				img3 = new ImageIcon(this.getClass().getResource("../bungalowMaison.jpg")).getImage();
			}break;
			case "Caravane" : {
				img1 = new ImageIcon(this.getClass().getResource("../caravaneCh1.jpg")).getImage();
				img2 = new ImageIcon(this.getClass().getResource("../caravaneCh2.jpg")).getImage();
				img3 = new ImageIcon(this.getClass().getResource("../caravaneCuisine.jpg")).getImage();
			}break;
		}
		
		Connection conn = SqliteConnexion.dbConnector();
		PreparedStatement pst = null;
		ResultSet rst = null;
		String query = null;
		switch(Menu.type) {
			case "Appartement" : query = "select * from appartement where n_appartement='"+Appartement.n_app+"' ";
			break;
			case "Bungalow" : query = "select * from bungalow where n_bungalow='"+Bungalows.n_app+"' ";
			break;
			case "Caravane" : query = "select * from caravane where n_caravane='"+Caravanes.n_app+"' ";
			break;
			default : JOptionPane.showMessageDialog(null, "Aucun menu selectionné");
			break;
		}
		try {
			pst = conn.prepareStatement(query);
			//pst.setInt(1,Appartement.n_app);
			rst = pst.executeQuery();
			if(!rst.next()) {
				JOptionPane.showMessageDialog(null, "Selectionner une ligne");
				Appartement app = new Appartement();
				app.setVisible(true);
				return;
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e);
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 675);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtAppartement = new JTextField();
		txtAppartement.setBackground(new Color(255, 239, 213));
		txtAppartement.setFont(new Font("Tahoma", Font.PLAIN, 20));
		txtAppartement.setEditable(false);
		txtAppartement.setText(Menu.type);
		txtAppartement.setBounds(642, 163, 311, 45);
		contentPane.add(txtAppartement);
		txtAppartement.setColumns(10);
		
		textFieldCapacite = new JTextField();
		textFieldCapacite.setBackground(new Color(255, 239, 213));
		textFieldCapacite.setEditable(false);
		try {
			textFieldCapacite.setText(rst.getString("capacite"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "noooooooon");
		}
		textFieldCapacite.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldCapacite.setColumns(10);
		textFieldCapacite.setBounds(645, 218, 148, 31);
		contentPane.add(textFieldCapacite);
		
		textFieldDispo = new JTextField();
		textFieldDispo.setBackground(new Color(255, 239, 213));
		textFieldDispo.setEditable(false);
		try {
			textFieldDispo.setText(rst.getString("disponibilite"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "noooooooon");
		}
		textFieldDispo.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldDispo.setColumns(10);
		textFieldDispo.setBounds(645, 331, 125, 31);
		contentPane.add(textFieldDispo);
		
		textFieldLocali = new JTextField();
		textFieldLocali.setBackground(new Color(255, 239, 213));
		textFieldLocali.setEditable(false);
		try {
			textFieldLocali.setText(rst.getString("localisation"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "noooooooon");
		}
		textFieldLocali.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldLocali.setColumns(10);
		textFieldLocali.setBounds(645, 392, 196, 31);
		contentPane.add(textFieldLocali);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1002, 619);
		panel.setBackground(new Color(173, 216, 230));
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("DHs");
		lblNewLabel_1.setBounds(790, 282, 48, 20);
		panel.add(lblNewLabel_1);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel_1.setForeground(Color.WHITE);
		
		textFieldTarif = new JTextField();
		textFieldTarif.setBackground(new Color(255, 239, 213));
		textFieldTarif.setBounds(645, 273, 125, 36);
		panel.add(textFieldTarif);
		textFieldTarif.setEditable(false);
		try {
			textFieldTarif.setText(rst.getString("prix"));
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		textFieldTarif.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldTarif.setColumns(10);
		
		JLabel lblType = new JLabel("Type : ");
		lblType.setBounds(488, 161, 103, 52);
		panel.add(lblType);
		lblType.setForeground(SystemColor.inactiveCaptionBorder);
		lblType.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 25));
		
		JLabel lblCapacit = new JLabel("Capacit\u00E9 : ");
		lblCapacit.setBounds(439, 214, 152, 44);
		panel.add(lblCapacit);
		lblCapacit.setForeground(SystemColor.inactiveCaptionBorder);
		lblCapacit.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 25));
		
		JLabel lblTarifDuneNuit = new JLabel("Tarif d'une nuit : ");
		lblTarifDuneNuit.setBounds(354, 261, 234, 54);
		panel.add(lblTarifDuneNuit);
		lblTarifDuneNuit.setForeground(SystemColor.inactiveCaptionBorder);
		lblTarifDuneNuit.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 25));
		
		JLabel lblDisponibilit = new JLabel("Disponible : ");
		lblDisponibilit.setBounds(414, 318, 177, 43);
		panel.add(lblDisponibilit);
		lblDisponibilit.setForeground(SystemColor.inactiveCaptionBorder);
		lblDisponibilit.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 25));
		
		JLabel lblLocalisation = new JLabel("Localisation : ");
		lblLocalisation.setBounds(414, 380, 177, 52);
		panel.add(lblLocalisation);
		lblLocalisation.setForeground(SystemColor.inactiveCaptionBorder);
		lblLocalisation.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 25));
		
		JLabel lblDetailsDeLappartement = new JLabel("Details de "+Menu.type);
		lblDetailsDeLappartement.setBounds(525, 16, 426, 52);
		panel.add(lblDetailsDeLappartement);
		lblDetailsDeLappartement.setForeground(Color.YELLOW);
		lblDetailsDeLappartement.setFont(new Font("SansSerif", Font.BOLD, 35));
		
		JLabel label_3 = new JLabel("");
		label_3.setBounds(33, 559, 48, 44);
		panel.add(label_3);
		label_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				switch(Menu.type) {
					case "Appartement" : {
						Appartement app1 = new Appartement();
						app1.setVisible(true);
						//payement.setVisible(false);
					}
					break;
					case "Bungalow" : {
						Bungalows bung = new Bungalows();
						bung.setVisible(true);
						//payement.setVisible(false);
					}
					break;
					case "Caravane" : {
						Caravanes caravane = new Caravanes();
						caravane.setVisible(true);
						//payement.setVisible(false);
					}
					break;
					default : JOptionPane.showMessageDialog(null, "Aucun menu selectionné");
					break;
				}
			}
		});
		label_3.setIcon(new ImageIcon(img4));
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(0, 0, 337, 208);
		panel.add(lblNewLabel);
		lblNewLabel.setIcon(new ImageIcon(img1));
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(0, 214, 354, 126);
		panel.add(label_1);
		label_1.setIcon(new ImageIcon(img2));
		
		JLabel label_2 = new JLabel("");
		label_2.setBounds(0, 356, 372, 199);
		panel.add(label_2);
		label_2.setIcon(new ImageIcon(img3));
		
		JButton btnModifier = new JButton("Modifier");
		btnModifier.setBounds(686, 500, 152, 44);
		panel.add(btnModifier);
		btnModifier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				switch(Menu.type) {
					case "Appartement" : {
						Appartement app1 = new Appartement();
						app1.setVisible(true);
						detailFram.setVisible(false);
						//payement.setVisible(false);
					}
					break;
					case "Bungalow" : {
						Bungalows bung = new Bungalows();
						bung.setVisible(true);
						detailFram.setVisible(false);
						//payement.setVisible(false);
					}
					break;
					case "Caravane" : {
						Caravanes caravane = new Caravanes();
						caravane.setVisible(true);
						detailFram.setVisible(false);
						//payement.setVisible(false);
					}
					break;
					default : JOptionPane.showMessageDialog(null, "Aucun menu selectionné");
					break;
				}
			}
		});
		btnModifier.setForeground(Color.BLACK);
		btnModifier.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnModifier.setBackground(new Color(255, 239, 213));
		
		JButton btnValider = new JButton("Valider");
		btnValider.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Login.email == null) {
					Reservation reserv = new Reservation();
					reserv.setVisible(true);
					detailFram.setVisible(false);
				}
				else {
					Modification modif = new Modification();
					modif.setVisible(true);
					detailFram.setVisible(false);
				}
			}
		});
		btnValider.setForeground(Color.BLACK);
		btnValider.setFont(new Font("Tahoma", Font.BOLD, 20));
		btnValider.setBackground(new Color(255, 239, 213));
		btnValider.setBounds(439, 500, 152, 44);
		panel.add(btnValider);
		
		JLabel label = new JLabel("");
		label.setBackground(Color.RED);
		label.setBounds(0, 0, 871, 542);
		//label.setBackground(new Color(173, 216, 230));
		//label.setIcon(new ImageIcon(img4));
		contentPane.add(label);
	}
}
