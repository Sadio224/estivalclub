package estival;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import com.toedter.calendar.JDateChooser;

public class Consultation extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNumeroApp;
	private JTextField textFieldCapacite;
	private JTextField textFieldTarif;
	private static JTextField textFieldNom;
	private static JTextField textFieldPrenom;
	private JTextField textFieldMail;
	private JTextField textFieldTel;
	private JTextField textFieldPays;
	private JTextField textFieldVille;
	private JTextField textFieldNbJour;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textFieldAdress;
	private final ButtonGroup buttonGroup_1 = new ButtonGroup();
	
	
	public static int numHebergement ;
	public static String mail ;
	public static String capacite ;
	public static String tarif ;
	public static String nom;
	public static String prenom;
	public static int nbjour;
	
	public static String typeHeb;
	public static int numApp;
	private JTextField textFieldArrivee;
	private JTextField textFieldSortie;

	/**
	 * Create the frame.
	 */
	public Consultation() {
		
		Consultation consult = this;
		
		Connection con1 = SqliteConnexion.dbConnector();
		String requette = "select * from clients where mail = ?";
		PreparedStatement ps1 = null;
		ResultSet rs1 = null;
		try {
			ps1 = con1.prepareStatement(requette);
			ps1.setString(1, Login.email);
			rs1 = ps1.executeQuery();
			if(!rs1.next()) {
				JOptionPane.showMessageDialog(null,  "Erreur : selection");
				Login login = new Login();
				login.setVisible(true);
				//modifiction.setVisible(false)
;			}
		} catch (SQLException e3) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null,  "from clients");
		}
		
		
		// Recuperation du type d'hebergement et de so numero
		try {
			typeHeb = rs1.getString("typeHebergement");
			numApp = Integer.parseInt(rs1.getString("numHebergement"));
		} catch (SQLException e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
		
		Image img = new ImageIcon(this.getClass().getResource("../background.png")).getImage();
		Image img4 = new ImageIcon(this.getClass().getResource("../back.png")).getImage();
		Image img7 = new ImageIcon(this.getClass().getResource("../back1.png")).getImage();
		Image img5 = new ImageIcon(this.getClass().getResource("../Close.png")).getImage();
		Image img6 = new ImageIcon(this.getClass().getResource("../Ok_48px.png")).getImage();
		
		
		
		
		
		
		Connection conn = SqliteConnexion.dbConnector();
		PreparedStatement pst = null;
		ResultSet rst = null;
		String query = null;
		switch(typeHeb) {
			case "Appartement" : query = "select * from appartement where n_appartement='"+numApp+"' ";
			break;
			case "Bungalow" : query = "select * from bungalow where n_bungalow='"+numApp+"' ";
			break;
			case "Caravane" : query = "select * from caravane where n_caravane='"+numApp+"' ";
			break;
			default : JOptionPane.showMessageDialog(null, "Aucun menu selectionn�");
			break;
		}
		try {
			pst = conn.prepareStatement(query);
			//pst.setInt(1,Appartement.n_app);
			rst = pst.executeQuery();
			if(!rst.next()) {
				JOptionPane.showMessageDialog(null, "Aucun appartement choisi");
				Appartement app = new Appartement();
				app.setVisible(true);
				return;
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, e);
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 740);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel label_1 = new JLabel("");
		label_1.setBounds(0, 70, 259, 161);
		//label_1.setIcon(new ImageIcon(img1));
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("");
		label_2.setBounds(0, 232, 259, 231);
		//label_2.setIcon(new ImageIcon(img2));
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("");
		label_3.setBounds(0, 463, 259, 184);
		//label_3.setIcon(new ImageIcon(img3));
		contentPane.add(label_3);
		
		JLabel lblEntrezVosInformations = new JLabel("Votre reservation");
		lblEntrezVosInformations.setBounds(295, 139, 411, 37);
		lblEntrezVosInformations.setForeground(Color.WHITE);
		lblEntrezVosInformations.setFont(new Font("Segoe UI Symbol", Font.BOLD, 22));
		contentPane.add(lblEntrezVosInformations);
		
		JLabel lblNApp = new JLabel("N\u00B0  : ");
		lblNApp.setBounds(269, 80, 71, 43);
		lblNApp.setForeground(new Color(255, 255, 51));
		lblNApp.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblNApp);
		
		textFieldNumeroApp = new JTextField();
		textFieldNumeroApp.setBounds(335, 84, 103, 31);
		textFieldNumeroApp.setText((String) null);
		textFieldNumeroApp.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldNumeroApp.setEditable(false);
		textFieldNumeroApp.setColumns(10);
		try {
			switch(typeHeb) {
			case "Appartement" : textFieldNumeroApp.setText(rst.getString("n_appartement"));
			break;
			case "Bungalow" : textFieldNumeroApp.setText(rst.getString("n_bungalow"));
			break;
			case "Caravane" : textFieldNumeroApp.setText(rst.getString("n_caravane"));
			break;
			default : JOptionPane.showMessageDialog(null, "Aucun menu selectionn�");
			break;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "noooooooon");
		}
		contentPane.add(textFieldNumeroApp);
		
		JLabel lblCapacit = new JLabel("Capacit\u00E9 : ");
		lblCapacit.setBounds(443, 80, 92, 43);
		lblCapacit.setForeground(new Color(255, 255, 51));
		lblCapacit.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblCapacit);
		
		textFieldCapacite = new JTextField();
		textFieldCapacite.setBounds(532, 84, 103, 31);
		textFieldCapacite.setText((String) null);
		textFieldCapacite.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldCapacite.setEditable(false);
		textFieldCapacite.setColumns(10);
		try {
			textFieldCapacite.setText(rst.getString("capacite"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "noooooooon");
		}
		contentPane.add(textFieldCapacite);
		
		JLabel lblTarif = new JLabel("Tarif : ");
		lblTarif.setBounds(650, 80, 64, 43);
		lblTarif.setForeground(new Color(255, 255, 51));
		lblTarif.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblTarif);
		
		textFieldTarif = new JTextField();
		textFieldTarif.setBounds(712, 84, 103, 31);
		textFieldTarif.setText((String) null);
		textFieldTarif.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldTarif.setEditable(false);
		textFieldTarif.setColumns(10);
		try {
			textFieldTarif.setText(rst.getString("prix"));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "noooooooon");
		}
		contentPane.add(textFieldTarif);
		
		JLabel lblDhs = new JLabel("DHs");
		lblDhs.setBounds(819, 80, 58, 43);
		lblDhs.setForeground(new Color(255, 255, 51));
		lblDhs.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblDhs);
		
		JLabel lblNom = new JLabel("Nom : ");
		lblNom.setBounds(274, 194, 71, 37);
		lblNom.setForeground(new Color(255, 255, 51));
		lblNom.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblNom);
		
		textFieldNom = new JTextField();
		textFieldNom.setBackground(new Color(255, 239, 213));
		textFieldNom.setBounds(348, 198, 162, 31);
		textFieldNom.setEditable(false);
		try {
			textFieldNom.setText(rs1.getString("nom"));
		} catch (SQLException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		textFieldNom.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldNom.setColumns(10);
		contentPane.add(textFieldNom);
		
		JLabel lblPrenom = new JLabel("Prenom : ");
		lblPrenom.setBounds(525, 194, 83, 37);
		lblPrenom.setForeground(new Color(255, 255, 51));
		lblPrenom.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblPrenom);
		
		textFieldPrenom = new JTextField();
		textFieldPrenom.setBackground(new Color(255, 239, 213));
		textFieldPrenom.setBounds(614, 198, 194, 31);
		textFieldPrenom.setEditable(false);
		try {
			textFieldPrenom.setText(rs1.getString("prenom"));
		} catch (SQLException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		textFieldPrenom.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldPrenom.setColumns(10);
		contentPane.add(textFieldPrenom);
	
		JLabel lblMail = new JLabel("Mail : ");
		lblMail.setBackground(new Color(255, 239, 213));
		lblMail.setBounds(274, 349, 66, 37);
		lblMail.setForeground(new Color(255, 255, 51));
		lblMail.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblMail);
		
		textFieldMail = new JTextField();
		textFieldMail.setBackground(new Color(255, 239, 213));
		textFieldMail.setBounds(348, 350, 457, 31);
		textFieldMail.setEditable(false);
		try {
			textFieldMail.setText(rs1.getString("mail"));
		} catch (SQLException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		textFieldMail.setFont(new Font("Tahoma", Font.BOLD, 16));
		textFieldMail.setColumns(10);
		contentPane.add(textFieldMail);
		
		JLabel lblTel = new JLabel("Tel : ");
		lblTel.setBounds(564, 247, 58, 37);
		lblTel.setForeground(new Color(255, 255, 51));
		lblTel.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblTel);
		
		textFieldTel = new JTextField();
		textFieldTel.setBackground(new Color(255, 239, 213));
		textFieldTel.setBounds(614, 251, 194, 31);
		textFieldTel.setEditable(false);
		try {
			textFieldTel.setText(rs1.getString("tel"));
		} catch (SQLException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		textFieldTel.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldTel.setColumns(10);
		contentPane.add(textFieldTel);
		
		JLabel lblPays = new JLabel("Pays : ");
		lblPays.setBounds(284, 247, 71, 37);
		lblPays.setForeground(new Color(255, 255, 51));
		lblPays.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblPays);
		
		textFieldPays = new JTextField();
		textFieldPays.setBackground(new Color(255, 239, 213));
		textFieldPays.setBounds(348, 248, 162, 31);
		textFieldPays.setEditable(false);
		try {
			textFieldPays.setText(rs1.getString("pays"));
		} catch (SQLException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		textFieldPays.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldPays.setColumns(10);
		contentPane.add(textFieldPays);
		
		JLabel lblVille = new JLabel("Ville : ");
		lblVille.setBounds(281, 296, 59, 37);
		lblVille.setForeground(new Color(255, 255, 51));
		lblVille.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblVille);
		
		textFieldVille = new JTextField();
		textFieldVille.setBackground(new Color(255, 239, 213));
		textFieldVille.setBounds(348, 302, 162, 31);
		textFieldVille.setEditable(false);
		try {
			textFieldVille.setText(rs1.getString("ville"));
		} catch (SQLException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		textFieldVille.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldVille.setColumns(10);
		contentPane.add(textFieldVille);
		
		JLabel lblArrive = new JLabel("Arriv\u00E9e : ");
		lblArrive.setBounds(256, 479, 83, 37);
		lblArrive.setForeground(new Color(255, 255, 51));
		lblArrive.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblArrive);
		
		JLabel lblSortie = new JLabel("Sortie : ");
		lblSortie.setBounds(571, 479, 71, 37);
		lblSortie.setForeground(new Color(255, 255, 51));
		lblSortie.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblSortie);
		
		JLabel lblNbJour = new JLabel("Nb Jour :");
		lblNbJour.setBounds(564, 545, 83, 37);
		lblNbJour.setForeground(new Color(255, 255, 51));
		lblNbJour.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblNbJour);
		
		textFieldNbJour = new JTextField();
		textFieldNbJour.setBackground(new Color(255, 239, 213));
		textFieldNbJour.setBounds(668, 546, 137, 31);
		textFieldNbJour.setEditable(false);
		textFieldNbJour.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldNbJour.setColumns(10);
		try {
			textFieldNbJour.setText(rs1.getString("nbJour"));
		} catch (SQLException e4) {
			// TODO Auto-generated catch block
			e4.printStackTrace();
		}
		contentPane.add(textFieldNbJour);
		
		JLabel lblAdresse = new JLabel("Adresse : ");
		lblAdresse.setBounds(256, 410, 83, 37);
		lblAdresse.setForeground(new Color(255, 255, 51));
		lblAdresse.setFont(new Font("Tahoma", Font.BOLD, 16));
		contentPane.add(lblAdresse);
		
		textFieldAdress = new JTextField();
		textFieldAdress.setBackground(new Color(255, 239, 213));
		textFieldAdress.setBounds(348, 409, 459, 35);
		textFieldAdress.setEditable(false);
		try {
			textFieldAdress.setText(rs1.getString("adresse"));
		} catch (SQLException e3) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "erreru adresse");
		}
		textFieldAdress.setFont(new Font("Tahoma", Font.BOLD, 20));
		textFieldAdress.setColumns(10);
		contentPane.add(textFieldAdress);
		try {
			rst.close();
			pst.close();
			conn.close();
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1002, 57);
		panel.setBackground(new Color(135, 206, 235));
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblBack = new JLabel("");
		lblBack.setBounds(0, 0, 48, 52);
		panel.add(lblBack);
		lblBack.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				EspaceUtilisateur espu = new EspaceUtilisateur();
				espu.setVisible(true);
				consult.setVisible(false);
			}
		});
		lblBack.setIcon(new ImageIcon(img4));
		
		JButton btnQuitter = new JButton("Quitter");
		btnQuitter.setBounds(346, 598, 457, 43);
		btnQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnQuitter.setForeground(Color.WHITE);
		btnQuitter.setBorder(new LineBorder(Color.WHITE, 2, true));
		btnQuitter.setContentAreaFilled(false);
		btnQuitter.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnQuitter.setBackground(new Color(51, 102, 255));
		btnQuitter.setIcon(new ImageIcon(img5));
		contentPane.add(btnQuitter);
		
		textFieldArrivee = new JTextField();
		textFieldArrivee.setBackground(new Color(255, 239, 213));
		textFieldArrivee.setBounds(364, 481, 146, 31);
		textFieldArrivee.setFont(new Font("Tahoma", Font.BOLD, 18));
		textFieldArrivee.setEditable(false);
		try {
			textFieldArrivee.setText(rs1.getString("dateArrivee"));
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e2);
		}
		contentPane.add(textFieldArrivee);
		textFieldArrivee.setColumns(10);
		
		textFieldSortie = new JTextField();
		textFieldSortie.setBackground(new Color(255, 239, 213));
		textFieldSortie.setBounds(649, 479, 156, 31);
		textFieldSortie.setFont(new Font("Tahoma", Font.BOLD, 18));
		textFieldSortie.setEditable(false);
		textFieldSortie.setColumns(10);
		try {
			textFieldSortie.setText(rs1.getString("dateSortie"));
		} catch (SQLException e2) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, e2);
		}
		contentPane.add(textFieldSortie);
		
		
		JLabel label = new JLabel("");
		label.setBounds(0, 56, 1002, 608);
		label.setIcon(new ImageIcon(img7));
		contentPane.add(label);
		
		try {
			ps1.close();
			rs1.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			JOptionPane.showMessageDialog(null, "Erreur : Fermure de rs1 et ps1");
		}
	}
}
