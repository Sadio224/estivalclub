package estival;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.UIManager;
import java.awt.Font;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.AncestorEvent;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.border.LineBorder;
import javax.swing.border.EtchedBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Menu extends JFrame {

	private JPanel contentPane;
	public static String type;


	/**
	 * Create the frame.
	 */
	public Menu() {
		
		Menu menu = this;
		
		if(Login.login == null && Login.email == null) {
			JOptionPane.showMessageDialog(null, "Creer un compte");
			Estival est = new Estival();
			est.getFrame().setVisible(true);
			this.dispose();
			return;
		}
		
		Image img = new ImageIcon(this.getClass().getResource("../back1.png")).getImage();
		Image img1 = new ImageIcon(this.getClass().getResource("../icon_exit_16.png")).getImage();
		Image img2 = new ImageIcon(this.getClass().getResource("../bed2.jpg")).getImage();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 675);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(img));
		lblNewLabel.setBounds(0, 0, 478, 633);
		contentPane.add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(173, 216, 230));
		panel.setBounds(0, 0, 1006, 628);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBorder(null);
		panel_1.setBounds(544, 235, 391, 330);
		panel.add(panel_1);
		panel_1.setBackground(new Color(255,255,255,40));
		panel_1.setLayout(null);
		
		JButton button = new JButton("Appartement");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button.setBackground(new Color(255, 255,255, 30));
			}
		});
		button.setBorder(new LineBorder(new Color(255, 255, 255), 2, true));
		button.setContentAreaFilled(false);
		button.setBounds(113, 68, 179, 52);
		panel_1.add(button);
		button.setFont(new Font("Tahoma", Font.BOLD, 16));
		button.setForeground(new Color(250, 128, 114));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Menu.type = "Appartement";
				//JOptionPane.showMessageDialog(null, "appartement");
				Appartement appart = new Appartement();
				appart.setVisible(true);
				menu.setVisible(false);
				
			}
		});
		button.setBackground(new Color(139, 69, 19));
		
		JButton btnCaravane = new JButton("Caravane");
		btnCaravane.setBorder(new LineBorder(Color.WHITE, 2, true));
		btnCaravane.setContentAreaFilled(false);
		btnCaravane.setBounds(113, 149, 179, 52);
		panel_1.add(btnCaravane);
		btnCaravane.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Menu.type = "Caravane";
				Caravanes carav = new Caravanes();
				carav.setVisible(true);
				menu.setVisible(false);
			}
		});
		btnCaravane.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnCaravane.setForeground(new Color(250, 128, 114));
		btnCaravane.setBackground(new Color(139, 69, 19));
		
		JButton btnNewButton = new JButton("Bungalow");
		btnNewButton.setBorder(new LineBorder(Color.WHITE, 2, true));
		btnNewButton.setContentAreaFilled(false);
		btnNewButton.setBounds(113, 239, 179, 52);
		panel_1.add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Menu.type = "Bungalow";
				Bungalows bgl = new Bungalows();
				bgl.setVisible(true);
				menu.setVisible(false);
			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 16));
		btnNewButton.setForeground(new Color(250, 128, 114));
		btnNewButton.setBackground(new Color(139, 69, 19));
		
		JLabel lblMenu = new JLabel("MENU");
		lblMenu.setForeground(new Color(250, 128, 114));
		lblMenu.setFont(new Font("Century Gothic", Font.BOLD | Font.ITALIC, 50));
		lblMenu.setBounds(671, 176, 154, 73);
		panel.add(lblMenu);
		
		JLabel label_1 = new JLabel("Estival reservation");
		label_1.setForeground(Color.BLACK);
		label_1.setFont(new Font("Andora", Font.BOLD | Font.ITALIC, 60));
		label_1.setBounds(512, 13, 464, 115);
		panel.add(label_1);
		
		JButton btnXQuitter = new JButton("Quitter");
		btnXQuitter.setBorder(null);
		btnXQuitter.setContentAreaFilled(false);
		btnXQuitter.setBounds(900, 585, 106, 43);
		panel.add(btnXQuitter);
		btnXQuitter.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnXQuitter.setForeground(Color.BLACK);
		btnXQuitter.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnXQuitter.setBackground(new Color(51, 102, 255));
		
		JLabel lblQuestceQuiVous = new JLabel("Qu'est-ce qui vous convient ?");
		lblQuestceQuiVous.setBounds(626, 238, 278, 52);
		panel.add(lblQuestceQuiVous);
		lblQuestceQuiVous.setForeground(Color.BLACK);
		lblQuestceQuiVous.setFont(new Font("Quentin", Font.PLAIN, 25));
		
		JLabel lblNewLabel_1 = new JLabel("WELCOME HERE !");
		lblNewLabel_1.setBounds(544, 98, 410, 45);
		panel.add(lblNewLabel_1);
		lblNewLabel_1.setForeground(new Color(255, 239, 213));
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 45));
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(img1));
		label.setBounds(896, 585, 22, 43);
		panel.add(label);
		
		
	}
}
