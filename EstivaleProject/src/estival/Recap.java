package estival;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.toedter.calendar.JDayChooser;
import com.toedter.calendar.JDateChooser;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.beans.PropertyChangeEvent;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.SwingConstants;

public class Recap extends JFrame {

	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	
	public void printComponenet(Component component){
		  PrinterJob pj = PrinterJob.getPrinterJob();
		  pj.setJobName(" Print Component ");

		  pj.setPrintable (new Printable() {    
		    public int print(Graphics pg, PageFormat pf, int pageNum){
		      if (pageNum > 0){
		      return Printable.NO_SUCH_PAGE;
		      }

		      Graphics2D g2 = (Graphics2D) pg;
		      g2.translate(pf.getImageableX(), pf.getImageableY());
		      component.paint(g2);
		      return Printable.PAGE_EXISTS;
		    }
		  });
		  if (pj.printDialog() == false)
		  return;

		  try {
		        pj.print();
		  } catch (PrinterException ex) {
		        // handle exception
		  }
		}
	
	public Recap() {
		
		Image img = new ImageIcon(this.getClass().getResource("../Print_48px.png")).getImage();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 675);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(135, 206, 235));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(255, 255, 255));
		panel.setBounds(250, 62, 587, 398);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JButton btnEnvoie = new JButton("Imprim\u00E9");
		btnEnvoie.setVerticalAlignment(SwingConstants.TOP);
		btnEnvoie.setFont(new Font("Tahoma", Font.BOLD, 21));
		btnEnvoie.setForeground(new Color(0, 0, 0));
		btnEnvoie.setBackground(Color.LIGHT_GRAY);
		btnEnvoie.setBounds(451, 493, 238, 53);
		btnEnvoie.setIcon(new ImageIcon(img));
		btnEnvoie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				/*EnvoieEmail.main(tab);*/
				//MessageFormat header = new MessageFormat("Récépissé de reservation club Estival");
				//MessageFormat footer = new MessageFormat("Page");
				printComponenet(panel);
				Login login = new Login();
				login.setVisible(true);
					
			}
		});
		contentPane.add(btnEnvoie);
		
		
		
		JLabel lblRcpissDeReservation = new JLabel("R\u00E9c\u00E9piss\u00E9 de reservation");
		lblRcpissDeReservation.setForeground(new Color(102, 51, 0));
		lblRcpissDeReservation.setFont(new Font("Tahoma", Font.BOLD, 23));
		lblRcpissDeReservation.setBounds(138, 38, 300, 37);
		panel.add(lblRcpissDeReservation);
		
		JLabel label_1 = new JLabel("Nom :");
		label_1.setBounds(203, 106, 69, 20);
		panel.add(label_1);
		
		JLabel labelNom = new JLabel(Payement.nom);
		labelNom.setFont(new Font("Tahoma", Font.BOLD, 18));
		labelNom.setBounds(328, 106, 69, 20);
		panel.add(labelNom);
		
		JLabel labelPrenom = new JLabel(Payement.prenom);
		labelPrenom.setFont(new Font("Tahoma", Font.BOLD, 18));
		labelPrenom.setBounds(328, 142, 150, 20);
		panel.add(labelPrenom);
		
		JLabel label_4 = new JLabel("Prenom :");
		label_4.setBounds(183, 142, 80, 20);
		panel.add(label_4);
		
		JLabel label_5 = new JLabel("Type Hebergement :");
		label_5.setBounds(105, 178, 150, 20);
		panel.add(label_5);
		
		JLabel label_6 = new JLabel("Capacit\u00E9 :");
		label_6.setBounds(184, 214, 80, 20);
		panel.add(label_6);
		
		JLabel labelType = new JLabel(Payement.typeHeberg);
		labelType.setFont(new Font("Tahoma", Font.BOLD, 18));
		labelType.setBounds(328, 178, 117, 20);
		panel.add(labelType);
		
		JLabel labelCapacite = new JLabel(""+Payement.capacite);
		labelCapacite.setFont(new Font("Tahoma", Font.BOLD, 18));
		labelCapacite.setBounds(328, 214, 85, 20);
		panel.add(labelCapacite);
		
		JLabel labelTarif = new JLabel(""+Payement.tarif);
		labelTarif.setFont(new Font("Tahoma", Font.BOLD, 18));
		labelTarif.setBounds(328, 256, 54, 20);
		panel.add(labelTarif);
		
		JLabel labelNbJour = new JLabel(""+Payement.nbJour);
		labelNbJour.setFont(new Font("Tahoma", Font.BOLD, 18));
		labelNbJour.setBounds(328, 292, 85, 20);
		panel.add(labelNbJour);
		
		JLabel label_11 = new JLabel("Nb Jour :");
		label_11.setBounds(187, 292, 85, 20);
		panel.add(label_11);
		
		JLabel label_12 = new JLabel("Tarif nuit :");
		label_12.setBounds(178, 256, 85, 20);
		panel.add(label_12);
		
		JLabel lblDhs = new JLabel("DHs");
		lblDhs.setBounds(409, 257, 69, 20);
		panel.add(lblDhs);
		
		JLabel lblClubEstival = new JLabel("Club Estival");
		lblClubEstival.setForeground(new Color(102, 51, 51));
		lblClubEstival.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblClubEstival.setBounds(460, 362, 112, 20);
		panel.add(lblClubEstival);
	}
}
