package estival;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.mysql.cj.protocol.Resultset;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class Inscription extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldNom;
	private JTextField textFieldPrenom;
	private JTextField textFieldLogin;
	private JTextField textField_3;


	/**
	 * Create the frame.
	 */
	public Inscription() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 740);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(135, 206, 235));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNom = new JLabel(" Nom :");
		lblNom.setBounds(281, 201, 70, 31);
		contentPane.add(lblNom);
		
		JLabel lblPrenom = new JLabel("Prenom :");
		lblPrenom.setBounds(266, 260, 80, 31);
		contentPane.add(lblPrenom);
		
		JLabel lblNewLabel = new JLabel("Mail :");
		lblNewLabel.setBounds(281, 318, 69, 43);
		contentPane.add(lblNewLabel);
		
		JLabel lblPassword = new JLabel("Password :");
		lblPassword.setBounds(258, 377, 98, 31);
		contentPane.add(lblPassword);
		
		textFieldNom = new JTextField();
		textFieldNom.setBounds(417, 203, 297, 26);
		contentPane.add(textFieldNom);
		textFieldNom.setColumns(10);
		
		textFieldPrenom = new JTextField();
		textFieldPrenom.setBounds(417, 262, 297, 26);
		contentPane.add(textFieldPrenom);
		textFieldPrenom.setColumns(10);
		
		textFieldLogin = new JTextField();
		textFieldLogin.setColumns(10);
		textFieldLogin.setBounds(417, 326, 297, 26);
		contentPane.add(textFieldLogin);
		
		textField_3 = new JTextField();
		textField_3.setColumns(10);
		textField_3.setBounds(417, 379, 297, 26);
		contentPane.add(textField_3);
		
		JLabel lblInscription = new JLabel("Inscription");
		lblInscription.setFont(new Font("Tahoma", Font.BOLD, 25));
		lblInscription.setBounds(346, 62, 265, 31);
		contentPane.add(lblInscription);
		
		JButton btnNewButton = new JButton("Enregistrer");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Connection conn = SqliteConnexion.dbConnector();
			    PreparedStatement pst;
				try {
					pst = conn.prepareStatement("insert into client(nom, username,password) values('"+textFieldNom.getText()+"','"+textFieldPrenom.getText()+"','"+textFieldNom+"')");
					int rse = 0;
					rse = pst.executeUpdate();
					if(rse != 0) {
						Login log = new Login();
						log.setVisible(true);
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(553, 518, 161, 49);
		contentPane.add(btnNewButton);
		
		JButton btnAnnuler = new JButton("Annuler");
		btnAnnuler.setBounds(221, 518, 173, 49);
		contentPane.add(btnAnnuler);
	}

}
