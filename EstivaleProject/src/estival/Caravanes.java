package estival;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import net.proteanit.sql.DbUtils;

public class Caravanes extends JFrame {
	
	static public PreparedStatement pst = null;
	static public ResultSet rs = null;
	String query = null;
	public static int n_app = -1;
	public static String type;

	private JPanel contentPane;
	private JTable table;

	// Affichage des données de la base dans la table
		public void displayData(ResultSet rs, PreparedStatement pst) {
				
			try {
				
				table.setModel(DbUtils.resultSetToTableModel(rs));
				
			} catch(Exception e) {
				JOptionPane.showConfirmDialog(null, e);
			}
			finally {
				try {
					pst.close();
					rs.close();
				}catch(Exception e) {
					JOptionPane.showMessageDialog(null, e);
				}
			}
		}
	
	/**
	 * Create the frame.
	 */
	Connection conn=SqliteConnexion.dbConnector();
	public Caravanes() {
		
		//Recuperation 
		type = "Caravane";
		Menu.type = this.type;
		
		Caravanes caravane = this;
		
		query="select * from caravane";
	    try {
			pst=conn.prepareStatement(query);
			rs = pst.executeQuery();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	    
	    Image img7 = new ImageIcon(this.getClass().getResource("../back1.png")).getImage();
		Image img = new ImageIcon(this.getClass().getResource("../background.png")).getImage();
		Image img2 = new ImageIcon(this.getClass().getResource("../Home_48px.png")).getImage();
		Image img3 = new ImageIcon(this.getClass().getResource("../Close.png")).getImage();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 675);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			}
		});
		scrollPane.setBounds(15, 138, 712, 363);
		contentPane.add(scrollPane);
		
		table = new JTable();
		table.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				int row = table.getSelectedRow();
				n_app = (int)(table.getModel().getValueAt(row, 0));
			}
		});
		scrollPane.setViewportView(table);
		
		//Affichage dans la table
		 displayData(rs,pst);
		
		JPanel panel = new JPanel();
		panel.setBackground(new Color(135, 206, 235));
		panel.setBounds(0, 0, 1002, 32);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JButton button = new JButton("Appartement");
		button.setForeground(Color.WHITE);
		button.setFont(new Font("Tahoma", Font.BOLD, 16));
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Menu.type = "Appartement";
				Appartement app = new Appartement();
				app.setVisible(true);
				caravane.setVisible(false);
			}
		});
		button.setBackground(new Color(139, 69, 19));
		button.setBounds(0, 0, 150, 32);
		panel.add(button);
		
		JButton button_1 = new JButton("Bungalow");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Menu.type = "Bungalow";
				Bungalows bung = new Bungalows();
				bung.setVisible(true);
				caravane.setVisible(false);
			}
		});
		button_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		button_1.setForeground(Color.WHITE);
		button_1.setBackground(new Color(139, 69, 19));
		button_1.setBounds(147, 0, 123, 32);
		panel.add(button_1);
		
		JButton button_2 = new JButton("Caravane");
		button_2.setEnabled(false);
		button_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		button_2.setForeground(Color.WHITE);
		button_2.setBackground(new Color(139, 69, 19));
		button_2.setBounds(266, 0, 116, 32);
		panel.add(button_2);
		
		JLabel lblReservationDappartement = new JLabel("Reservation de Caravane");
		lblReservationDappartement.setFont(new Font("SansSerif", Font.BOLD, 30));
		lblReservationDappartement.setForeground(new Color(255, 255, 0));
		lblReservationDappartement.setBounds(97, 34, 426, 52);
		contentPane.add(lblReservationDappartement);
		
		JLabel lblListeDesAppartements = new JLabel("Liste des Caravanes disponibles");
		lblListeDesAppartements.setFont(new Font("Sylfaen", Font.BOLD, 18));
		lblListeDesAppartements.setForeground(Color.WHITE);
		lblListeDesAppartements.setBounds(76, 102, 294, 20);
		contentPane.add(lblListeDesAppartements);
		
		JLabel lblNewLabel = new JLabel("Capacit\u00E9 :");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNewLabel.setBounds(743, 169, 116, 40);
		contentPane.add(lblNewLabel);
		
		JComboBox comboBoxCapacite = new JComboBox();
		comboBoxCapacite.setForeground(Color.WHITE);
		comboBoxCapacite.setBackground(SystemColor.textHighlight);
		comboBoxCapacite.setFont(new Font("Tahoma", Font.BOLD, 18));
		comboBoxCapacite.setBounds(873, 172, 73, 33);
		comboBoxCapacite.addItem(2);
		comboBoxCapacite.addItem(4);
		comboBoxCapacite.addItem(6);
		contentPane.add(comboBoxCapacite);
		
		JLabel lblRestauration = new JLabel("Prix Max :");
		lblRestauration.setForeground(Color.WHITE);
		lblRestauration.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblRestauration.setBounds(742, 226, 116, 40);
		contentPane.add(lblRestauration);
		
		JComboBox comboBoxMaxPrix = new JComboBox();
		comboBoxMaxPrix.setForeground(Color.WHITE);
		comboBoxMaxPrix.setBackground(SystemColor.textHighlight);
		comboBoxMaxPrix.setFont(new Font("Tahoma", Font.BOLD, 18));
		comboBoxMaxPrix.setBounds(873, 225, 73, 40);
		comboBoxMaxPrix.addItem(300);
		comboBoxMaxPrix.addItem(400);
		comboBoxMaxPrix.addItem(600);
		comboBoxMaxPrix.addItem(800);
		contentPane.add(comboBoxMaxPrix);
		
		
		JButton btnActualiser = new JButton("Rechercher");
		btnActualiser.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnActualiser.setForeground(Color.WHITE);
		btnActualiser.setBackground(SystemColor.textHighlight);
		btnActualiser.setContentAreaFilled(false);
		btnActualiser.setBorder(new LineBorder(Color.WHITE, 2, true));
		btnActualiser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//conn=SqliteConnexion.dbConnector();
				query = "select * from caravane where capacite=? and prix <=?";
				try {
					pst = conn.prepareStatement(query);
					pst.setInt(1, (int)comboBoxCapacite.getSelectedItem() );
					pst.setFloat(2, (int)comboBoxMaxPrix.getSelectedItem());
					rs = pst.executeQuery();
					//if(rs.next()) {
						//JOptionPane.showMessageDialog(null, rs.getString(3));
						
						displayData(rs,pst) ;
						
					/*}
					else
						JOptionPane.showMessageDialog(null, "noooooon");*/
				}catch(Exception e) {
					JOptionPane.showMessageDialog(null, e);
				}
				
			}
		});
		btnActualiser.setBounds(743, 282, 204, 40);
		contentPane.add(btnActualiser);
		
		JButton btnDetails = new JButton("Details App");
		btnDetails.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (n_app != -1) {
					Detail detail = new Detail();
					detail.setVisible(true);
				}
				else
					JOptionPane.showMessageDialog(null, "Aucune ligne selectionnée");
			}
		});
		btnDetails.setForeground(Color.WHITE);
		btnDetails.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnDetails.setContentAreaFilled(false);
		btnDetails.setBorder(new LineBorder(Color.WHITE, 2, true));
		btnDetails.setBackground(SystemColor.textHighlight);
		btnDetails.setBounds(742, 345, 204, 40);
		contentPane.add(btnDetails);
		
		JButton btnBookApp = new JButton("Book App");
		btnBookApp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (n_app != -1) {
					if(Login.email == null) {
						Reservation reserv = new Reservation();
						reserv.setVisible(true);
					}
					else {
						Modification modif = new Modification();
						modif.setVisible(true);
					}
				}
				else
					JOptionPane.showMessageDialog(null, "Aucune ligne selectionnée");
			}
		});
		btnBookApp.setForeground(Color.WHITE);
		btnBookApp.setContentAreaFilled(false);
		btnBookApp.setBorder(new LineBorder(Color.WHITE, 2, true));
		btnBookApp.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnBookApp.setBackground(SystemColor.textHighlight);
		btnBookApp.setBounds(742, 411, 204, 40);
		contentPane.add(btnBookApp);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				Menu menu = new Menu();
				menu.setVisible(true);
			}
		});
		lblNewLabel_1.setBounds(15, 34, 53, 52);
		lblNewLabel_1.setIcon(new ImageIcon(img2));
		contentPane.add(lblNewLabel_1);
		
		JLabel lbl1 = new JLabel("");
		lbl1.setIcon(new ImageIcon(img7));
		lbl1.setBounds(0, 34, 1002, 585);
		contentPane.add(lbl1);
	}

}
