package estival;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class EnvoieEmail {

	/*public static void main(String[] args) {

        final String username = "barry.abdoulayesadio@uit.ac.ma";
        final String password = "17008930";

        Properties prop = new Properties();
		prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true"); //TLS
        
        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("barry.abdoulayesadio@uit.ac.ma"));
            message.setRecipients(
                    Message.RecipientType.TO,
                    InternetAddress.parse("sadiobarry1993@gmail.com")
            );
            message.setSubject("Testing Gmail TLS");
            message.setText("Dear Mail Crawler,"
                    + "\n\n Please do not spam my email!");

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            e.printStackTrace();
        }
    } */

	//String tab[] = {"hello", "barry@barry.com"};
	public static void main(String[] args) {
		
		    final String username = "barry.abdoulayesadio@uit.ac.ma";
	        final String password = "17008930";

	        Properties prop = new Properties();
			prop.put("mail.smtp.host", "smtp.gmail.com");
	        prop.put("mail.smtp.port", "587");
	        prop.put("mail.smtp.auth", "true");
	        prop.put("mail.smtp.starttls.enable", "true"); //TLS
	        
	        Session session = Session.getInstance(prop,
	                new javax.mail.Authenticator() {
	                    protected PasswordAuthentication getPasswordAuthentication() {
	                        return new PasswordAuthentication(username, password);
	                    }
	                });

	        try {

	            Message message = new MimeMessage(session);
	            message.setFrom(new InternetAddress("barry.abdoulayesadio@uit.ac.ma"));
	            message.setRecipients(
	                    Message.RecipientType.TO,
	                    InternetAddress.parse(args[1])
	            );
	            message.setSubject("Reservation d'hebergement clube Estival");
	            message.setText(args[0]);

	            Transport.send(message);

	            //System.out.println("Done");

	        } catch (MessagingException e) {
	            e.printStackTrace();
	        }
	}
}
