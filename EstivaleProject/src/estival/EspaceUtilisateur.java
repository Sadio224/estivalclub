package estival;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

public class EspaceUtilisateur extends JFrame {

	private JPanel contentPane;


	/**
	 * Create the frame.
	 */
	public EspaceUtilisateur() {
		
		EspaceUtilisateur espU = this;
		
		Image img = new ImageIcon(this.getClass().getResource("../user.png")).getImage();
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1024, 675);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(135, 206, 235));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Espace utilisateur");
		lblNewLabel.setForeground(Color.WHITE);
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 35));
		lblNewLabel.setBounds(278, 16, 372, 41);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Consulter");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Consultation consult = new Consultation();
				consult.setVisible(true);
				espU.setVisible(false);
			}
		});
		btnNewButton.setBackground(new Color(0, 102, 255));
		btnNewButton.setForeground(new Color(255, 255, 255));
		btnNewButton.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnNewButton.setBounds(635, 167, 240, 54);
		contentPane.add(btnNewButton);
		
		JButton btnModifier = new JButton("Modifier reservation");
		btnModifier.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Menu menu = new Menu();
				menu.setVisible(true);
				espU.setVisible(false);
			}
		});
		btnModifier.setBackground(new Color(0, 102, 255));
		btnModifier.setForeground(new Color(255, 255, 255));
		btnModifier.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnModifier.setBounds(635, 260, 240, 54);
		contentPane.add(btnModifier);
		
		//Rendre l'hebergement disponible et suppression dans la liste des clients
		JButton btnAnnuler = new JButton(" Annuler reservation");
		btnAnnuler.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Connection conn4 = SqliteConnexion.dbConnector();
				String requette = null;
				try {
					PreparedStatement ps4 = conn4.prepareStatement("select numHebergement, typeHebergement from clients where mail = '"+Login.email+"'");
					ResultSet rs4 = ps4.executeQuery();
					PreparedStatement ps5 = null;
					PreparedStatement ps6 = null;
					int res5, res6;
					if(rs4.next()) {
						switch(rs4.getString("typeHebergement")) {
							case "Appartement" : requette = "update appartement set disponibilite = 'oui' where n_appartement = '"+rs4.getInt("numHebergement")+"'";
							break;
							case "Bungalow" : requette = "update bungalow set disponibilite = 'oui' where n_bungalow = '"+rs4.getInt("numHebergement")+"'";
							break;
							case "Caravane" : requette = "update caravane set disponibilite = 'oui' where n_caravane = '"+rs4.getInt("numHebergement")+"'";
							break;
							default : JOptionPane.showMessageDialog(null, "Aucun menu selectionné");
							break;
						}
						ps5 = conn4.prepareStatement(requette);
						if(ps5.executeUpdate() == 0) {
							JOptionPane.showMessageDialog(null, "Erreur de mise a jour");
						}
						else
							ps5.close();
					}
					else {
						JOptionPane.showMessageDialog(null, "Erreur de selection");
						Login login = new Login();
						login.setVisible(true);
					}
					
					ps6 = conn4.prepareStatement("delete from clients where mail = '"+Login.email+"'");
					res6 = ps6.executeUpdate();
					ps6.close();
					ps4.close();
					rs4.close();
					conn4.close();
					Menu menu = new Menu();
					menu.setVisible(true);
					espU.setVisible(false);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnAnnuler.setBackground(new Color(0, 102, 255));
		btnAnnuler.setForeground(new Color(255, 255, 255));
		btnAnnuler.setFont(new Font("Tahoma", Font.BOLD, 18));
		btnAnnuler.setBounds(635, 356, 240, 54);
		contentPane.add(btnAnnuler);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(160, 131, 278, 327);
		lblNewLabel_1.setIcon(new ImageIcon(img));
		contentPane.add(lblNewLabel_1);
	}
}
